from AbstractMenuItemScreen import AbstractMenuItemScreen
from time import sleep


__author__ = 'diego'

class ShutdownScreen(AbstractMenuItemScreen):

    def __init__(self, aContent, aDisplay):
        super(ShutdownScreen,self).__init__("Shutdown\nSystem...", aDisplay)
        self.set_backlight_colour_to_red()
        self._defaultContent = self.get_a_pretty_line("Press \'select\'") +"\n"+ self.get_a_pretty_line("to shutdown")

    def execute_command_SELECT(self):
        self.set_backlight_colour_to_red()
        self.run_shell_command("mpc stop")
        self._defaultContent = "Wait 10 seconds\nthen switch off"
        self.update_content()
        sleep(4)
        self.run_shell_command("sudo shutdown -h now")
        sleep(1.5)
        self._lcd_display.noDisplay()
        self._lcd_display.backlight(self._lcd_display.OFF)
        return self