#!/usr/bin/python

from time import sleep
from subprocess import *
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

class TextScroller:
        'Class for scrolling text'
        text = ''
        position = 0
        textLength = 0

        def __init__(self, initialText, textLength):
                self.text = initialText
                self.textLength = textLength

        def scroll(self):
                doubleText = self.text + '     ' + self.text
                scrolledText = doubleText[self.position:len(doubleText)]
                self.position = self.position + 1

                # We add five extra spaces between each complete text scroll.
                if self.position > len(self.text) + 4 :
                        self.position = 0

                return scrolledText

        def setNewText(self, newText):
                self.text = newText
                self.position = 0


# Initialize the LCD plate.  Should auto-detect correct I2C bus.  If not,
# pass '0' for early 256 MB Model B boards or '1' for all later versions
lcd = Adafruit_CharLCDPlate()
lcd.begin(16,2)
scroller = TextScroller('', 16)

# Clear display and show greeting, pause 1 sec
lcd.clear()
lcd.backlight(lcd.WHITE)
lcd.message("Ciao pallina\npremi un tasto!")
sleep(1)

# Cycle through backlight colors
col = (lcd.RED , lcd.YELLOW, lcd.GREEN, lcd.TEAL,
       lcd.BLUE, lcd.VIOLET, lcd.ON   , lcd.OFF)
for c in col:
    lcd.backlight(c)
    sleep(1)

# Poll buttons, display message & set backlight accordingly
btn = ((lcd.LEFT  , 'Autobot Transformer!'              , lcd.YELLOW),
       (lcd.UP    , 'Sita sings the blues'     , lcd.BLUE),
       (lcd.DOWN  , 'I see fields of green'    , lcd.GREEN),
       (lcd.RIGHT , 'Purple mountain nmajesties', lcd.VIOLET),
       (lcd.SELECT, 'I miei padroni sono i Giugiclavadi', lcd.RED))
prev = -1
while True:
    lcd.setCursor(0,0)
    for b in btn:
        if lcd.buttonPressed(b[0]):
            if b is not prev:
                lcd.clear()
                scroller.setNewText(b[1])
#                lcd.message(b[1])
                lcd.backlight(b[2])
                prev = b
            break
        
    lcd.message(scroller.scroll()[0:16])
    sleep(0.25)

# Prova definizione metodo
    def run_cmd(cmd):
        p = Popen(cmd, shell=True, stdout=PIPE, stderr=STDOUT)
        output = p.communicate()[0]
        return output
