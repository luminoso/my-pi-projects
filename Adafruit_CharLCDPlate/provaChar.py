from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

# Initialize the LCD plate.  Should auto-detect correct I2C bus.  If not,
# pass '0' for early 256 MB Model B boards or '1' for all later versions
lcd = Adafruit_CharLCDPlate()
lcd.begin(16,2)
'''
# degree symbol
lcd.createChar(1, [0b01100,
                   0b10010,
                   0b10010,
                   0b01100,
                   0b00000,
                   0b00000,
                   0b00000,
                   0b00000])
'''
# vol symbol
addr1 = 0x01
addr2 = 0x02
addr3 = 0x03
addr4 = 0x04
addr5 = 0x05
addr6 = 0x06
'''
lcd.createChar(int(addr1), [0b00001,
                   0b00011,
                   0b01111,
                   0b01111,
                   0b01111,
                   0b00011,
                   0b00001,
                   0b00000])
lcd.createChar(int(addr1), [0b00000,
                            0b00000,
                            0b01110,
                            0b10001,
                            0b10101,
                            0b10001,
                            0b01110])
lcd.createChar(int(addr2), [0b01000,
                   0b10000,
                   0b00000,
                   0b11000,
                   0b00000,
                   0b10000,
                   0b01000,
                   0b00000])
'''
lcd.createChar(int(addr1), [0b00000,
                            0b00000,
                            0b01110,
                            0b11001,
                            0b10101,
                            0b10001,
                            0b01110])

lcd.createChar(int(addr2), [0b00000,
                            0b00000,
                            0b01110,
                            0b10011,
                            0b10101,
                            0b10001,
                            0b01110])

lcd.createChar(int(addr3), [0b00000,
                            0b00000,
                            0b01110,
                            0b10001,
                            0b10101,
                            0b10011,
                            0b01110])

lcd.createChar(int(addr4), [0b00000,
                            0b00000,
                            0b01110,
                            0b10001,
                            0b10101,
                            0b11001,
                            0b01110])

'''
# hourglass
lcd.createChar(2, [0b11111,
                   0b10001,
                   0b01110,
                   0b00100,
                   0b01010,
                   0b10001,
                   0b11111,
                   0b00000])
'''
'''
# play
lcd.createChar(int(addr3), [0b01000, #%1000
                   0b01100, #%1100
                   0b01110, #%1010
                   0b01111, #%1001
                   0b01110, #%1010
                   0b01100, #%1100
                   0b01000, #%1000
                   0b00000])
# stop
lcd.createChar(int(addr4), [0b00000,
                   0b11111,
                   0b11111,
                   0b11111,
                   0b11111,
                   0b11111,
                   0b00000,
                   0b00000])
# pause
lcd.createChar(int(addr5), [0b11011,
                   0b11011,
                   0b11011,
                   0b11011,
                   0b11011,
                   0b11011,
                   0b11011,
                   0b00000])
'''
animation = [addr1, addr2, addr3, addr4]
# Clear display and show greeting, pause 1 sec
lcd.clear()
lcd.backlight(lcd.WHITE)
#lcd.message("Ciao pallina \x0000 \npremi un tasto!")
#addr1 = "\x01"
#addr2 = chr(0x02)
#lcd.message("Volume: \x01\x02")
lcd.message("Volume: " + chr(addr1) + chr(addr2) + chr(addr3) + chr(addr4))
lcd.message("\n")
lcd.message("Play: "+ chr(addr3)+chr(addr4)+chr(addr5))
i = 0
j = 2
while True:
    lcd.clear()
    lcd.message("Volume: " + chr(addr1) + chr(addr2) + chr(addr3) + chr(addr4))
    lcd.message("\n")
    lcd.message("V"+ chr(animation[i])+"lumi"+chr(animation[j]))
    i = (i+1)%4
    j = (j+1)%4
    sleep(0.25)
